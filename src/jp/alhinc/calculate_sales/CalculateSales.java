package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
    private static final String TOTAL_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String SHOP_NUMBER_INVALID_FORMAT = "の支店コードが不正です";
	private static final String COMMODITY_NUMBER_INVALID_FORMAT = "の商品コードが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードとそれに対応する商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと商品ごとの売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, "支店定義ファイル", "^[0-9]{3}$", branchNames, branchSales)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, "商品定義ファイル", "^[0-9a-zA-Z]{8}$", commodityNames, commoditySales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			String formerFileName = rcdFiles.get(i).getName();
			String latterFileName = rcdFiles.get(i+1).getName();
			int former = Integer.parseInt(formerFileName.substring(0,8));
			int latter = Integer.parseInt(latterFileName.substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}
		BufferedReader br = null;
		//rcdFilesの数だけ繰り返す。ファイルを開くたび閉じる。
		for (File file : rcdFiles) {
			try {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				List<String> rcdList = new ArrayList<>();
				String line;
				while((line = br.readLine()) != null) {
					rcdList.add(line);
				}
				if(rcdList.size() != 3) {
					System.out.println(file.getName() + FILE_INVALID_FORMAT);
					return;
				}
				if (!branchSales.containsKey(rcdList.get(0))) {
					System.out.println(file.getName() + SHOP_NUMBER_INVALID_FORMAT);
					return;
				}
				if (!commoditySales.containsKey(rcdList.get(1))) {
					System.out.println(file.getName() + COMMODITY_NUMBER_INVALID_FORMAT);
					return;
				}
				if(!rcdList.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//支店別の売上金額集計
				long rcdSale = Long.parseLong(rcdList.get(2));
				Long saleAmount = branchSales.get(rcdList.get(0)) + rcdSale;
				//商品別の売上金額集計
				Long commodityAmount = commoditySales.get(rcdList.get(1)) + rcdSale;
				if(saleAmount >= 10000000000L || commodityAmount >= 10000000000L) {
					System.out.println(TOTAL_AMOUNT_OVER);
					return;
				}
				branchSales.put(rcdList.get(0), saleAmount);
				commoditySales.put(rcdList.get(1), commodityAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String japaneseFileName, String regularExpressions, Map<String, String> names, Map<String, Long>sales) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(japaneseFileName + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] nameData = line.split(",");
				if(nameData.length != 2 || !nameData[0].matches(regularExpressions)) {
					System.out.println(japaneseFileName + FILE_INVALID_FORMAT);
					return false;
				}
				names.put(nameData[0], nameData[1]);
				sales.put(nameData[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


}
